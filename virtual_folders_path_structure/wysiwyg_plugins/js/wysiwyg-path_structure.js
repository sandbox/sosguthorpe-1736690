(function ($) {
 
function addTheLink(text, link, instanceId) {
	
	if (link == "<front>") link = Drupal.settings.basePath;
	
	var content = '<a href="' + link + '" >' + text + '</a>';
	Drupal.wysiwyg.instances[instanceId].insert(content);
}

Drupal.wysiwyg.plugins['virtual_folders_path_structure'] = {
 
  /**
   * Invoke is called when the toolbar button is clicked.
   */
  invoke: function(data, settings, instanceId) {
     // Typically, an icon might be added to the WYSIWYG, which HTML gets added
     // to the plain-text version.
	 $(getLinkTreeModal($)).dialog( "option", "buttons",
		[{
    		text: "Search",
    		click: function() {
    			var text = prompt("Enter the word or phrase you wish to search for in the title. You can clear previous searches by pressing cancel.");
    			if (text != null && text != "") $(this).jstree("search", text);
    			else $(this).jstree("clear_search");
    		}
    	},
    	{
        	text: "Done",
        	click: function() {
        		// Selected link
        		var selected = $("a.selected", this);
        		
        		if (selected.length > 0) {
        			// grab the link
        			var link = $(selected).attr("rel");
        			
        			if (data.format == "html") {
              		  if ($(data.node).is('a')) {
          		        // Alter this link
              			$(data.node).attr("href", link);
          		      }
              		  
              		  else {
              			// Add a new link
              			var text = data.content;
              			if (text == undefined || text == "") {
              				text = $(selected).attr("title");
              				if (text == undefined || text == "") {
              					text = link;
              				}
              			}
              			addTheLink (text, link, instanceId);
              		  }
              		} else if (data.format == "text") {
              			var text = data.content;
              			if (text == undefined || text == "") {
              				text = link;
              			}
              			addTheLink (text, link, instanceId);
              		}
        		}
        		
        		// Hide the tree.
        		hideTree($);
        	} 
        }]
	 )
     .dialog('option', 'title', "Add a link");
	  
	 // Show the tree
	 showTree($);
   }
};
 
})(jQuery);