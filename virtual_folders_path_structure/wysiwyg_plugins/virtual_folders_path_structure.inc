<?php

function virtual_folders_path_structure_virtual_folders_path_structure_plugin() {
	
	// Add the necessary JS/CSS
  virtual_folders_path_structure_include_files();
  
  $wysiwyg_dir = drupal_get_path('module', 'virtual_folders_path_structure') . '/wysiwyg_plugins';
  
  // Plugin definition
  $plugins['virtual_folders_path_structure'] = array(
    'title' => t('Virtual folders path structure'),
    'icon path' => $wysiwyg_dir . '/images',
    'icon file' => 'wysiwyg-path_structure.gif',
    'icon title' => t('Insert Link'),
    'js path' => $wysiwyg_dir . '/js',
    'js file' => 'wysiwyg-path_structure.js',
    'css file' => NULL,
    'css path' => NULL,
    'settings' => array()
  );

  return $plugins;
}