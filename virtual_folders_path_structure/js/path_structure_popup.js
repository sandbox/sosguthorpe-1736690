// Extend the fileTree plugin

// Variable to hold the tree.
var modal_tree = null;

function getLinkTreeModal($) {
	if (modal_tree == null) {
		// Turn the div into a modal popup.
		modal_tree = $("<div>").attr("id", "path-structure-tree").attr("tabindex", "10000").dialog({
			modal: true,
			title: "Browse the site",
			autoOpen : false,
			closeOnEscape: true,
			select_node : false
		});
		
		// Create the tree.
		modal_tree.jstree({			
			"html_data" : {
				"ajax" : {
					"url" : "/virtual_folders/path_structure/ajax",
					"data" : function (n) {
						var a = $('a' , n);
						return { dir : a.attr ? a.attr("rel") : "" };
					}
				}
			},
			"ui" : {
				"select_limit" : 1
			},
			"types" : { 
				// This will prevent moving or creating any other type as a root node
				"valid_children" : [ "root" ],
				"types" : {
					"root" : {
						"max_children"	: -1,
						"max_depth"		: -1,
						"hover_node" : false,
						"valid_children" : ["folder", "selectable_folder", "file"]
					},
					"selectable_folder" : {
						"max_children"	: -1,
						"max_depth"		: -1,
						"valid_children" : ["folder", "selectable_folder", "file"]
					},
					"folder" : {
						"max_children"	: -1,
						"max_depth"		: -1,
						"valid_children" : ["folder", "selectable_folder", "file"],
						"hover_node" : false,
						"select_node" : false
					},
					"file" : {
						"max_children"	: 0,
						"max_depth"		: -1,
						"valid_children" : "none"
					}
				}
			},
			"themes" : {
				theme : "custom",
				url : "/" + Drupal.settings.virtual_folders.path + "/jstree_theme/custom/style.css"
			},
			"search" : {
				"ajax" : {
					"url" : "/virtual_folders/path_structure/ajax/search",
					"data" : function (s) {
						return { search : s };
					}
				}
			},
			"plugins" : [ "themes", "html_data", "ui", "types", "search"],
			"correct_state" : false
		});
	}
	
	// Return the tree.
	return modal_tree;
}

function showTree($) {
	
	// Show the Modal.
	$(getLinkTreeModal($)).dialog("open");
}

function hideTree($) {
	
	// Hide the Modal.
	$(getLinkTreeModal($)).dialog("close");
}

// Document ready behaviour.
jQuery(document).ready( function($) {
	
	// Bind the click event to the link tag.
	$(getLinkTreeModal($)).delegate('.selectable > a, .jstree-leaf > a', "click", function(e) {
		
		// Ensure only one selected element.
		$('a.selected', getLinkTreeModal($)).removeClass("selected");
		$(this).addClass("selected");
		
		// Return false to stop the event propagation.
		return false;
	});
});