<?php

class VF_Folder {
	public $id;
	public $path;
	public $name;
	
	public function __construct($id, $path, $name) {
		$this->id = $id;
		$this->path = $path;
		$this->name = $name;
	}
}